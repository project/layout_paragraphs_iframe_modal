/**
 * Implementation of the openIframe Ajax command.
 *
 * This command uses the default openDialog command to initialize a dialog. It
 * then attaches event listeners to the iframe and window to handle closing and
 * resizing the dialog.
 */
(function ($, Drupal) {

  $(document)
    .once('updateLocalTasks')
    .ready(function init() {
      waitForElm('.block.contextual-region li a.is-active.tabs__link--local-tasks').then((element) => {
        $(element).html('Save');
      });
    });

  function waitForElm(selector) {
    return new Promise(resolve => {
      if (document.querySelector(selector)) {
        return resolve(document.querySelector(selector));
      }

      const observer = new MutationObserver(mutations => {
        if (document.querySelector(selector)) {
          resolve(document.querySelector(selector));
          observer.disconnect();
        }
      });

      observer.observe(document.body, {
        childList: true,
        subtree: true
      });
    });
  }

  $(document)
    .once('saveContextualForm')
    .on('click', '.is-active.tabs__link--local-tasks', function(e){
      e.preventDefault();
      e.stopPropagation();
      $('form[data-drupal-selector] .form-actions input[type="submit"]').eq(0).click();
    });

})(jQuery, Drupal);
