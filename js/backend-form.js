/**
 * Implementation of the openIframe Ajax command.
 *
 * This command uses the default openDialog command to initialize a dialog. It
 * then attaches event listeners to the iframe and window to handle closing and
 * resizing the dialog.
 */
(function ($, Drupal) {

  $.extend({
    replaceTag: function (element, tagName, withDataAndEvents, deepWithDataAndEvents) {
      var newTag = $("<" + tagName + ">")[0];
      // From [Stackoverflow: Copy all Attributes](http://stackoverflow.com/a/6753486/2096729)
      $.each(element.attributes, function() {
        newTag.setAttribute(this.name, this.value);
      });
      $(element).children().clone(withDataAndEvents, deepWithDataAndEvents).appendTo(newTag);
      return newTag;
    }
  });
  $.fn.extend({
    replaceTag: function (tagName, withDataAndEvents, deepWithDataAndEvents) {
      // Use map to reconstruct the selector with newly created elements
      return this.map(function() {
        return jQuery.replaceTag(this, tagName, withDataAndEvents, deepWithDataAndEvents);
      })
    }
  });

  function waitForElm(selector) {
    return new Promise(resolve => {
      if (document.querySelector(selector)) {
        return resolve(document.querySelector(selector));
      }

      const observer = new MutationObserver(mutations => {
        if (document.querySelector(selector)) {
          resolve(document.querySelector(selector));
          observer.disconnect();
        }
      });

      observer.observe(document.body, {
        childList: true,
        subtree: true
      });
    });
  }

  waitForElm('input.dialog-cancel.form-submit').then((element) => {
    let value = $('input.dialog-cancel.form-submit').val();
    let replacedElement = $(element).replaceTag('a')[0];
    $(element).replaceWith(replacedElement);
    $(".dialog-cancel").text(value).click(function(){
      window.parent.postMessage('LBIM_DISMISS');
    });
  });

  $('body').removeClass('gin--classic-toolbar');

})(jQuery, Drupal);
