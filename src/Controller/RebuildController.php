<?php

declare(strict_types=1);

namespace Drupal\layout_paragraphs_iframe_modal\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\layout_paragraphs\Form\ComponentFormBase;
use Drupal\Core\Ajax\CloseDialogCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Routing\RouteMatch;
use Drupal\Core\Url;
use Drupal\layout_paragraphs\Ajax\LayoutParagraphsEventCommand;
use Drupal\layout_paragraphs\LayoutParagraphsLayout;
use Drupal\layout_paragraphs\LayoutParagraphsLayoutTempstoreRepository;
use Drupal\layout_paragraphs\LayoutParagraphsLayoutRefreshTrait;
use Drupal\layout_paragraphs_iframe_modal\Ajax\ScrollToBlockCommand;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a controller to rebuild the layout.
 */
class RebuildController implements ContainerInjectionInterface {

  use LayoutParagraphsLayoutRefreshTrait;

  /**
   * The layout tempstore repository.
   *
   * @var \Drupal\layout_paragraphs\LayoutParagraphsLayoutTempstoreRepository
   */
  protected $layoutTempstoreRepository;

  /**
   * RebuildController constructor.
   *
   * @param \Drupal\layout_paragraphs\LayoutParagraphsLayoutTempstoreRepository $layout_tempstore_repository
   *   The layout tempstore repository.
   */
  public function __construct(LayoutParagraphsLayoutTempstoreRepository $layout_tempstore_repository) {
    $this->layoutTempstoreRepository = $layout_tempstore_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('layout_paragraphs.tempstore_repository')
    );
  }

  /**
   * Rebuild the current layout and close the dialog.
   *
   * @param \Drupal\layout_paragraphs\LayoutParagraphsLayout $layout_paragraphs_layout
   *   The paragraphs layout.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   An AJAX response.
   */
  public function rebuild(LayoutParagraphsLayout $layout_paragraphs_layout): AjaxResponse {
    $this->layoutTempstoreRepository->set($layout_paragraphs_layout);
    $this->setLayoutParagraphsLayout($layout_paragraphs_layout);
    $response = new AjaxResponse();
    if ($this->needsRefresh()) {
      $response = $this->refreshLayout($response);
    }
    $request = \Drupal::request();
    $component_id = $request->attributes->get('layout_paragraphs_component');
    $paragraph_component = [
      '#type' => 'layout_paragraphs_builder',
      '#layout_paragraphs_layout' => $this->layoutParagraphsLayout,
      '#uuid' => $component_id,
    ];
    $response->addCommand(new ReplaceCommand("[data-uuid={$component_id}]", $paragraph_component));
    $response->addCommand(new LayoutParagraphsEventCommand($this->layoutParagraphsLayout, $component_id, 'component:update'));
    $response->addCommand(new CloseDialogCommand('#drupal-lbim-modal'));
    $response->addCommand(new ScrollToBlockCommand());
    return $response;
  }

  /**
   * Build an Url for this route.
   *
   * @param \Drupal\layout_paragraphs\LayoutParagraphsLayout $section_storage
   *   The section storage.
   * @param string $paragraphs_component
   *   The paragraphs component ID being edited.
   *
   * @return \Drupal\Core\Url
   *   Url for this route.
   */
  public static function buildRouteUrl(LayoutParagraphsLayout $paragraphs_layout, string $paragraphs_component): Url {
    $route = 'layout_paragraphs_iframe_modal.rebuild';
    $route_parameters = [
      'layout_paragraphs_layout' => $paragraphs_layout->id(),
      'layout_paragraphs_component' => $paragraphs_component
    ];

    return Url::fromRoute($route, $route_parameters);
  }

}
